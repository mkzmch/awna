# AWNA

## Description
**awna** stands for Automatic Wireless Network Access and it 
is an OpenBSD specific ksh script to manage wireless networks
it presents a list of available networks and allows the user to
connect them using either a gui based on zenity or a cli interface.

A few commands used by
**awna**
require special permissions
so it should be run as root. The .desktop file takes care of that 
automatically if you install a gui.
## Interface
______________________  ***CLI***___________________________________________________________ ***GUI***
![alt text](https://gitlab.com/mkzmch/awna/-/raw/main/images/interface.png)

## Installation and usage
AWNA comes with an installer. In order to install it run  
 ``` git clone https://gitlab.com/mkzmch/awna  ```  
 ``` cd awna ```  
 ``` doas ./configure  ```  
The gui is preety self-explanatory. If you use the cli version then just run awna as root and you get a list of networks.
Choose a network you are willing to connect to by typing a number corresponding to that network 
then enter the WPA key (will echo) choose whether you want DHCP or not and you are connected.

There is one thing to consider. If your interface was initially disabled it might take 
some time to start up and see all the networks and while the script automatically starts it for 
you it doesn't wait for it to wake up, so if you don't see any networks where you should just
wait for 5-10 seconds and rerun the script

## Warning

Be forwarned that this script is REALLY simple and somewhat dumb, so if you terminate it in the 
middle of configuring a network it might break your connection and you will have to edit the 
hostname files manually to restore functionality  

## Contributing
Contributions are always welcome and ecouraged. 
